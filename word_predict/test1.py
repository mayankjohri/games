from itertools import cycle

lst = cycle(list(range(10)))

class CustomCycle(object):
    def __init__(self, lst):
        self._lst = lst
        self._current = 0

    def __next__(self):
        self._current = self._current + 1
        return self._lst[self._current]


while(lst):
    del lst[1]


# import  curses
#
# def main() -> int:
#     return curses.wrapper(c_main)
#
#
# def c_main(stdscr: 'curses.__CursesWindow') -> int:
#     name = ""
#     name_done = False
#     msg= "Word Predict Version: 0.0.1"
#     curses.use_default_colors()
#     stdscr.addstr(msg, curses.A_REVERSE)
#     stdscr.chgat(-1, curses.A_REVERSE)
#     quest_window = curses.newwin(curses.LINES-2, curses.COLS, 1, 0)
#     quest_text_window = quest_window.subwin(curses.LINES-6, curses.COLS-4, 3, 2)
#     quest_window.box()
#     stdscr.noutrefresh()
#     quest_window.noutrefresh()
#     curses.doupdate()
#     while True:
#         c = quest_window.getch()
#         if c == ord('r') or c == ord('R'):
#             quest_text_window.clear()
#             quest_text_window.addstr("Welcome")
#         if c == ord('Q'):
#             break
#         stdscr.noutrefresh()
#         quest_window.noutrefresh()
#         quest_text_window.noutrefresh()
#         curses.doupdate()
#     # word = "Toast"
#     # size = len(word)
#     # sol = ['_'] * size
#     # flg = True
#     # while flg:
#     #     quest_window.refresh()
#     #     quest_text.clear()
#     #     quest_text.addstr(f"Welcome\nPlease enter the word, its a {size} length word")
#     #
#     #     for char in sol:
#     #         quest_text.insstr(10, 4, char)
#     #         # quest_text.insstr(11, 4, curses.hline("-"))
#     #     char = quest_window.getch()
#     #     if char == ord('Q') :
#     #         flg = False
#     #         break
#     #         # quest_text.clear()
#     #         # quest_text.addstr("working")
#     #         # quest_text.refresh()
#     #         # quest_text.clear()
#     #     elif char == curses.KEY_BACKSPACE:
#     #         flg = False
#     #         # elif c == ord('q') or c == ord('Q'):
#     #         break
#     #     else:
#     #         quest_text.insstr(10,10, str(char))
#     #         pass
#     # stdscr.noutrefresh()
#     # quest_window.noutrefresh()
#     # curses.doupdate()
#     # # import time
#     # # time.sleep(1)
#     # # stdscr.move(0, len(msg))
#     # # if name_done:
#     # #     stdscr.addstr(1,0, f"Welcome {name}")
#     # # # Rendering phase
#     # #
#     # # char = stdscr.get_wch()
#     # # if isinstance(char, str) and char.isprintable():
#     # #     name +=char
#     # # elif char == curses.KEY_BACKSPACE:
#     # #     name = name[:-1]
#     # # else:
#     # #     raise(AssertionError(char))
#
#     return 0
#
#
# if __name__ == '__main__':
#     exit(main())
#
