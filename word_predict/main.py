"""

This game will take a word from the dictionary and scramble it and we have 30 seconds to find the word

"""
from string import ascii_uppercase
import os
import sys
import json
import curses
from curses import wrapper
from random import choice, sample

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()


def find(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]


def main():
    while True:
        if (letter := input("Please enter an Engish char to start: ").strip().upper()) in ascii_uppercase:
            break
    print(f"Reading dictionary with starting char: {letter}")
    file_name = os.path.join("data", f"D{letter}.json")
    with open(file_name) as dict_file:
        json_data = json.load(dict_file)
    words = tuple(json_data.keys())

    word = choice(words).lower()
    # print(word)
    size = len(word)
    progress = ["_"] * size
    tried = 0
    while "_" in progress and tried < 10:
        print(f"{tried=}")
        for char in progress:
            print(char, end=" ")
        print("")
        char = getch()
        print(char)
        if char  == "Q":
            break
        elif char in word:
            locations = find(word, char)
            for loc in locations:
                progress[loc] = char
        else:
            tried +=1

            # Lets find the location and update progress.
        # print(f"Scrambled work: {''.join(sample(word,len(word)))}")
        # gussed_word = input("Please enter the correct word: ").strip().upper()
        # if gussed_word == word:
        #     print("Good Job")
        # else:
        #     print(f"Better luck next time: {word=} ")
        # if input("Press `q` for exit or any any other key to continue: ").strip() == 'q':
        #     break
        #
    if "_" in progress:
        print(f"Sorry, better luck next time: {word=}")
    else:
        print(f"Great, gussed the right word: {word=}")
if __name__ == "__main__":
    main()
