/*
    License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    (C): Mayank Johri, 2021
*/
extern crate termion;
// use crate::termion::input::TermRead;

use rand::seq::SliceRandom;
use rand::Rng;
use std::io::{stdin, stdout, Write};
use term_basics_linux as tbl;

use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

// use getch::Getch;
use termion::{clear, color, cursor, style};

fn get_random_element(max_number: usize) -> usize {
    let secret_number = rand::thread_rng().gen_range(1..max_number);
    return secret_number;
}

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect()
}

fn get_rand_word() -> String {
    let file_name = "data/common_words.txt";
    let lines = lines_from_file(file_name);
    // println!("{:?}", lines.len() );
    let size = lines.len();
    let rand_num = get_random_element(size);
    let word = &lines[rand_num];
    return word.to_string();
}

fn shuffle_index(str_len: usize) -> Vec<usize> {
    let mut secret_num: Vec<_> = (0..str_len).collect();
    let mut rng = rand::thread_rng();

    secret_num.shuffle(&mut rng);

    // println!("{:?}", secret_num);
    return secret_num;
}

fn title(msg: String) {
    println!("{}{}", style::Reset, clear::All);
    let (w, _h) = termion::terminal_size().unwrap();
    let width = w as usize;
    println!(
        "{}{}{}{:^width$}{}",
        cursor::Goto(1, 0),
        color::Fg(color::Black),
        color::Bg(color::AnsiValue::grayscale(22)),
        msg,
        style::Reset,
        width = width
    );
}

fn get_scrambled_random_word() -> [String; 2] {
    let rand_word = get_rand_word();
    let _user_word = String::new();
    // println!("{:?}", rand_word);
    let rand_indexs = shuffle_index(rand_word.len());
    // println!("{:?}", rand_indexs );
    let mut stack = Vec::<String>::new();
    for index in rand_indexs {
        stack.push(rand_word.chars().nth(index).unwrap().to_string());
    }
    let _shuffled_word = stack.join("");
    return [rand_word, _shuffled_word];
}

fn getch() -> bool {
    let x = tbl::getch();
    if x == 113 || x == 81 {
        return true;
    }

    return false;
}

fn main() {
    let mut flg = false;
    while flg == false {
        let stdout = stdout();

        let mut stdout = stdout.lock();

        title("!!! Welcome to Word Scrambler, ver: 0.0.2 !!! ".to_string());
        let words = get_scrambled_random_word();
        println!("");
        println!(
            "{}{}Scrambled Word: {} {}",
            color::Fg(color::Magenta),
            style::Bold,
            style::Reset,
            words[1]
        );
        println!("");
        print!("Please enter the currect word: ");
        stdout.flush().unwrap();
        let mut user_word = String::new();

        stdin().read_line(&mut user_word).ok();
        user_word = user_word.trim().to_string();
        let user_word = user_word.replace(|c: char| !c.is_ascii(), "");
        if user_word == words[0] {
            println!("{bold}Congratulations{reset},\n   You typed: {blue}{}{reset}\n Actual word: {green}{}{reset}",
        user_word, words[0], bold=style::Bold, green=color::Fg(color::Green),
        blue=color::Fg(color::Blue), reset=style::Reset);
        } else {
            println!("{bold}Sorry{reset},  You typed: {blue}{}{reset} and the actual word was {green}{}{reset}",
        user_word, words[0], bold=style::Bold, green=color::Fg(color::Green),
        blue=color::Fg(color::Blue), reset=style::Reset);
        };
        println!("Press `q` to quit or press any other key on keyboard to try agian");
        flg = getch();
    }
    // println!("{lighgreen}-- src/test/ui/borrow-errors.rs at 82:18 --\n\
    //       {red}error: {reset}{bold}two closures require unique access to `vec` at the same time {reset}{bold}{magenta}[E0524]{reset}\n\
    //       {line_num_fg}{line_num_bg}79 {reset}     let append = |e| {{\n\
    //       {line_num_fg}{line_num_bg}{info_line}{reset}                  {red}^^^{reset} {error_fg}first closure is constructed here\n\
    //       {line_num_fg}{line_num_bg}80 {reset}         vec.push(e)\n\
    //       {line_num_fg}{line_num_bg}{info_line}{reset}                 {red}^^^{reset} {error_fg}previous borrow occurs due to use of `vec` in closure\n\
    //       {line_num_fg}{line_num_bg}84 {reset}     }};\n\
    //       {line_num_fg}{line_num_bg}85 {reset} }}\n\
    //       {line_num_fg}{line_num_bg}{info_line}{reset} {red}^{reset} {error_fg}borrow from first closure ends here",
    //      lighgreen = color::Fg(color::LightGreen),
    //      red = color::Fg(color::Red),
    //      bold = style::Bold,
    //      reset = style::Reset,
    //      magenta = color::Fg(color::Magenta),
    //      line_num_bg = color::Bg(color::AnsiValue::grayscale(3)),
    //      line_num_fg = color::Fg(color::AnsiValue::grayscale(18)),
    //      info_line = "|  ",
    //      error_fg = color::Fg(color::AnsiValue::grayscale(17)))
    // let stdout = stdout();
    // let mut stdout = stdout.lock();
    // let stdin = stdin();
    // let mut stdin = stdin.lock();
    // println!("{}",clear::All);

    // let r = 0x70;
    // let c = color::Rgb(r, !r, 2 * ((r % 128) as i8 - 64).abs() as u8);

    // println!("{}{}", cursor::Goto(1, 1), color::Bg(c));

    // println!("{}Red", color::Fg(color::Red));
    // println!("{}Blue", color::Fg(color::Blue));
    // println!("{}Blue'n'Bold{}", style::Bold, style::Reset);
    // println!("{}Just plain italic{}", style::Italic, style::Reset);
    // stdout.write_all(b"password: ").unwrap();
    // stdout.flush().unwrap();

    // let pass = stdin.read_passwd(&mut stdout);

    // if let Ok(Some(pass)) = pass {
    //     stdout.write_all(pass.as_bytes()).unwrap();
    //     stdout.write_all(b"\n").unwrap();
    // } else {
    //     stdout.write_all(b"Error\n").unwrap();
    // }
}
