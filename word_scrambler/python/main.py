"""

This game will take a word from the dictionary and scramble it and we have 30 seconds to find the word

"""
from string import ascii_uppercase
import os
import json
import curses
from curses import wrapper
from random import choice, sample

def main():
    while True:
        if (letter := input("Please enter an Engish char to start: ").strip().upper()) in ascii_uppercase:
            break
    print(f"Reading dictionary with starting char: {letter}")
    file_name = os.path.join("data", f"D{letter}.json")
    with open(file_name) as dict_file:
        json_data = json.load(dict_file)
    words = tuple(json_data.keys())
    while True:
        word = choice(words)
        # print(word)
        print(f"Scrambled work: {''.join(sample(word,len(word)))}")
        gussed_word = input("Please enter the correct word: ").strip().upper()
        if gussed_word == word:
            print("Good Job")
        else:
            print(f"Better luck next time: {word=} ")
        if input("Press `q` for exit or any any other key to continue: ").strip() == 'q':
            break


if __name__ == "__main__":
    main()
