from random import choice, sample

def main():
    with open('dictionary.txt') as f:
        words = f.readlines()
    while True:
        word = choice(words).strip().upper()
        # print(word)
        print(f"Scrambled work: {''.join(sample(word,len(word)))}")
        gussed_word = input("Please enter the correct word: ").strip().upper()
        if gussed_word == word:
            print("Good Job")
        else:
            print(f"Better luck next time: {word=} ")
        if input("Press `q` for exit or any any other key to continue: ").strip() == 'q':
            break


if __name__ == "__main__":
    main()

